var app = angular.module('app', ['ui.router']);

// routes
app.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider

        .state('home', {
            url: '/home',
            templateUrl: 'template/partial-home.html'
        })
        
     
        .state('popular', {
            url: '/book/popular',
            views: {
                '': { templateUrl: 'template/partial-book.html' },
                'column@popular': { 
                    templateUrl: 'template/table-data.html',
                    controller: 'allBookController'
                }
            }
        })
        .state('login', {
            url: '/login',
            views: {
                '': { 
                    templateUrl: 'template/partial-login.html',
                    controller: 'LoginController'
                }
            }
        })
        .state('register', {
            url: '/register',
            views: {
                '': {
                    templateUrl: 'template/partial-register.html',
                    controller: 'RegisterController'
                }
            }
        })
        .state('view', {
            url: '/book/view/:id',
            views: {
                '': { templateUrl: 'template/partial-book-id.html' },
                'row@view': { 
                    templateUrl: 'template/table-data-id.html',
                    controller: 'ViewController'
                }
            }
        });
});
// Book controller
app.controller('allBookController', function($scope, $http) {
    
    $http({
        method: 'GET',
        url: 'https://cde305resit-oyewoleo.c9users.io/book/popular'
    }).then(function successCallback(data) {
         $scope.books = data.data.data;
         $scope.total = $scope.books.length;
    }, function errorCallback(data) {
         $scope.books = data;
    });
    
});

app.controller('ViewController', function($scope, $http, $stateParams) {
    $http({
        method: 'GET',
        url: 'https://cde305resit-oyewoleo.c9users.io/book/view/'+$stateParams.id
    }).then(function successCallback(data) {
         $scope.books = data.data.data;
    }, function errorCallback(data) {
         $scope.books = data;
    });
    
});
// register
app.controller('RegisterController', function($scope, $http, $location) {
    $scope.saveData = function(username, password) {
        $scope.username = username;
        $scope.password = password;
        $http({
            method: 'POST',
            url: 'https://cde305resit-oyewoleo.c9users.io/register',
            data: {username: $scope.username, password: $scope.password},
        }).success(function (data) {
        });
    $location.path("/login");
    }
    
});
//login controller
app.controller('LoginController', function($scope, $http, $location) {
    $scope.saveData = function(username, password) {
        $scope.username = username;
        $scope.password = password;
        $http({
            method: 'POST',
            url: 'https://cde305resit-oyewoleo.c9users.io/login',
            data: {username: $scope.username, password: $scope.password},
        }).success(function (data) {
        });
     //When the user hits the login button it runs this function.
    $scope.encrypt = () => {
       //It stores the user and pass ng-model in its own variable.
        var username = $scope.user
        var password = $scope.pass
        //It then encodes it using a Base64 encryption mechanism, called btoa.
        var encoded = btoa(username + password)
        
        //This section is a debug method. I found out that registering an account or logging in an account, when no accounts exist breaks the client.
        //Where $scope objects wouldn't work, nor adding stuff to localStorage.
        //So here I have a dummy username and password. I call this my admin account. 
        var adminUser = 'admin'
        var adminPass = 'pass'
        //It encrypts it.
        var encodeAdmin = btoa(adminUser + adminPass)
        //But this will only be stored if the length of localStorage is less than 1. In some cases, when the browser opens for the first time, this will 
        //be likely to run
        if(localStorage.length < 1){
            localStorage.setItem(encodeAdmin, encodeAdmin)
        }
        
        //Anyway, if the user and pass ng-model when becoming encoded, it will check all items in the localStorage, if there is a match in the system
        for (var i in localStorage) {
            if(i == encoded){
                //If it does, it welcomes the username
                window.alert('Welcome back ' + username + ' !')
                $scope.message = 'Successful Login'
                //Then it updates this $rootScope I talk about. This updates the text to logged in as: <username>
                //This will be shown in all pages, until it refreshes, i.e logs out.
                $rootScope.test = 'Logged in as: ' + username
                $rootScope.logout = 'Logout'
                break;
            } else {
                //If it doesn't find a match, it tells the user in the html file that it is invalid
                $scope.message = 'Invalid Username or Password'

            }
        }
    }
    }
     
    
});

